package cucumberproject.services;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class UserService {
    private static final String UsersPath = "/users";

    public static Response getUsers() {
        return RestAssured
                .given()
                .contentType("application/json")
                .when()
                .get(UsersPath)
                .then()
                .extract()
                .response();
    }
}
