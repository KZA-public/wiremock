package cucumberproject.services;

import wiremock.com.fasterxml.jackson.databind.ObjectMapper;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class UserServiceStub {
    private static final String usersPath = "/users";

    public static void setUsersFromObject(Object responseObject) throws Throwable {
        ObjectMapper objectMapper = new ObjectMapper();
        String responseString = objectMapper.writeValueAsString(responseObject);

        stubFor(get(urlEqualTo(usersPath))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(responseString)));
    }

    public static void setUsersFromFile(String filename){
        stubFor(get(urlEqualTo(usersPath))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBodyFile(filename)));
    }
}
