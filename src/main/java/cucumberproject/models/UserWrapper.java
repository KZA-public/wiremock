package cucumberproject.models;

public class UserWrapper extends User {

    public static UserWrapper createDefault(){
        return new UserWrapper()
                .withName("Gebruiker")
                .withResidence("Baarn")
                .withJob("Software tester");
    }

    public UserWrapper withName(String name){
        this.setName(name);
        return this;
    }
    public UserWrapper withResidence(String residence){
        this.setResidence(residence);
        return this;
    }
    public UserWrapper withJob(String job){
        this.setJob(job);
        return this;
    }
}
