# language: nl
Functionaliteit: Wiremock stub

  Toelichting bij de scenarios:
  - Voorafgaand aan de test (in de zgn before hook) wordt mbv WireMock een Jetty server opgestart die later als stub zal fungeren.

    Specifiek voor scenario 1: Wiremock stub opgezet mbv een lijst gebruiker objecten
    In deze 'Gegeven' wordt een user lijst opgebouwd met -in dit geval- één user (usersStub).
    Dit wordt gedaan met behulp van een wrapper class om makkelijk een 'standaard' gebruiker aan te maken.
    Het gemaakte lijstje wordt op de response gemapped van http GET requests op '/0.0.0.0:8080/users'

    Specifiek voor scenario 2: Wiremock stub opgezet mbv een lijst gebruikers uit een json file
    In deze 'Gegeven' wordt een json bestandje uitgelezen (te vinden onder src/test/resources/__files)
    die vervolgens op de response wordt gemapped van http GET requests op '/0.0.0.0:8080/users'

  - In de 'Als' wordt met behulp van RestAssured dit GET request daadwerkelijk uitgevoerd.
    De response is daarmee in dit geval weer te parsen in hetzelfde lijstje aan gebruikers.
  - In de 'Dan' wordt dit lijstje van gebruikers gevalideert op aantal.
  - Na afloop van de test (in de zgn after hook) wordt de Jetty server weer uitgezet.

  Scenario: Wiremock stub opgezet mbv een lijst gebruiker objecten
    Gegeven de gebruiker "Gebruiker"
    Als ik alle gebruikers opvraag bij de gebruiker service
    Dan krijg ik "1" gebruikers terug

  Scenario: Wiremock stub opgezet mbv een lijst gebruikers uit een json file
    Gegeven de gebruikers uit file "TestGebruikers.json"
    Als ik alle gebruikers opvraag bij de gebruiker service
    Dan krijg ik "3" gebruikers terug