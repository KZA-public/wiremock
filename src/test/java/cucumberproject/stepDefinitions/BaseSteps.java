package cucumberproject.stepDefinitions;

import cucumberproject.models.UserWrapper;
import cucumberproject.models.User;
import java.util.List;

public class BaseSteps {
    protected static List<UserWrapper> usersStub;
    protected static List<User> usersResponse;
}
