package cucumberproject.stepDefinitions;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import com.github.tomakehurst.wiremock.WireMockServer;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumberproject.models.User;
import cucumberproject.models.UserWrapper;
import java.util.ArrayList;

public class Hooks extends BaseSteps{
    private WireMockServer wireMockServer;

    @Before
    public void init() {
        usersStub = new ArrayList<UserWrapper>();
        usersResponse = new ArrayList<User>();

        wireMockServer = new WireMockServer(options().port(8080));
        wireMockServer.start();
        //Start application
    }

    @After
    public void tearDown() {
        //Stop application
        wireMockServer.stop();
    }
}
