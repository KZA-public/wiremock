package cucumberproject.stepDefinitions;

import cucumber.api.java.nl.Als;
import cucumber.api.java.nl.Gegeven;
import cucumberproject.models.User;
import cucumberproject.models.UserWrapper;
import cucumberproject.services.UserService;
import cucumberproject.services.UserServiceStub;
import io.restassured.response.Response;
import org.junit.Assert;
import wiremock.com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;

public class WiremockSteps extends BaseSteps {

    @Gegeven("^de gebruiker \"([^\"]*)\"$")
    public void deGebruiker(String naam) throws Throwable {
        usersStub.add(UserWrapper.createDefault().withName(naam));
        UserServiceStub.setUsersFromObject(usersStub);
    }

    @Gegeven("^de gebruikers uit file \"([^\"]*)\"$")
    public void deGebruikersUitFile(String bestandsNaam) throws Throwable {
        UserServiceStub.setUsersFromFile(bestandsNaam);
    }

    @Als("^ik alle gebruikers opvraag bij de gebruiker service$")
    public void ikOpvraagBijDeGebruikerService() throws Throwable {
        Response response = UserService.getUsers();
        ObjectMapper mapper = new ObjectMapper();
        usersResponse = Arrays.asList(mapper.readValue(response.getBody().asString(), User[].class));
    }

    @Als("^krijg ik \"([^\"]*)\" gebruikers terug$")
    public void ikOpvraagBijDeGebruikerService(int aantal) throws Throwable {
        //Gehele response wordt gelogd in console
        for (User user : usersResponse){
            System.out.println(user.getName());
            System.out.println(user.getResidence());
            System.out.println(user.getJob());
        }
        Assert.assertEquals("Aantal gebruikers onjuist", aantal, usersResponse.size());
    }
}
